#Run the OpenACC code on Tesla GPU

!rm -f *.o radial_distribution_function
!nvc++ -acc -ta=tesla  -Minfo=accel -o radial_distribution_function radial_distribution_function.cpp -L/opt/nvidia/hpc_sdk/Linux_x86_64/21.3/cuda/11.2/lib64 -lnvToolsExt
!./radial_distribution_function  > output.txt
