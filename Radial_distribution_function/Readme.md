# OpenACC Radial distribution functions

Parallel implementation of Radial distribution functions in C

Includes sample output file and run script.

Compiled with Tesla GPU and Nvidia SDK

Ref:
(Radial distribution functions)[https://manual.gromacs.org/documentation/2021/reference-manual/analysis/radial-distribution-function.html]

(Radial Distribution Function, R.D.F.)[http://isaacs.sourceforge.net/phys/rdfs.html]
