
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <accelmath.h>
#include <cstring>
#include <cstdio>
#include <iomanip>
#include <assert.h>
#include "timer.h"
#include <fstream>

 
using namespace std;

void read_header(int *natom, int *nframes, std::istream &infile)
{

    infile.seekg(8, ios::beg);
    infile.read((char *)nframes, sizeof(int));
    infile.seekg(64 * 4, ios::cur);
    infile.read((char *)natom, sizeof(int));
    infile.seekg(1 * 8, ios::cur);
    return;
}



void read_data(double *x, double *y, double *z, std::istream &infile,
                  int natom, double &xbox, double &ybox, double &zbox)
{

    double d[6];
    for (int i = 0; i < 6; i++)
    {
        infile.read((char *)&d[i], sizeof(double));
    }
    xbox = d[0];
    ybox = d[2];
    zbox = d[5];
    float a, b, c;
    infile.seekg(1 * 8, ios::cur);
    for (int i = 0; i < natom; i++)
    {
        infile.read((char *)&a, sizeof(float));
        x[i] = a;
    }
    infile.seekg(1 * 8, ios::cur);
    for (int i = 0; i < natom; i++)
    {
        infile.read((char *)&b, sizeof(float));
        y[i] = b;
    }
    infile.seekg(1 * 8, ios::cur);
    for (int i = 0; i < natom; i++)
    {
        infile.read((char *)&c, sizeof(float));
        z[i] = c;
    }
    infile.seekg(1 * 8, ios::cur);

    return;
}






int round(float num)
{
    return num < 0 ? num - 0.5 : num + 0.5;
}

void kernel_pairwise_distance(const double *dev_x, const double *dev_y, const double *dev_z, unsigned long long int *dev_g2, 
                                        int numatm, int nconf, const double xbox, const double ybox, const double zbox, int d_bin)
{
    double cut;
    int ig2;
    double box;
    double del; 
    
    box = min(xbox, ybox);
    box = min(box, zbox);

    del = box / (2.0 * d_bin);
    cut = box * 0.5;
    
    #pragma acc data copyin (dev_x[:nconf * numatm], dev_y[:nconf * numatm], dev_z[:nconf * numatm]) copyout( dev_g2 [:2000])
    {
    
    #pragma acc parallel loop
    for (int frame = 0; frame < nconf; frame++)
    {
         #pragma acc loop
         for (int id1 = 0; id1 < numatm; id1++)
         {
             #pragma acc loop
             for (int id2 = 0; id2 < numatm; id2++)
             {
                 double r, dx, dy, dz;
                 
                 dx = dev_x[frame * numatm + id1] - dev_x[frame * numatm + id2];
                 dy = dev_y[frame * numatm + id1] - dev_y[frame * numatm + id2];
                 dz = dev_z[frame * numatm + id1] - dev_z[frame * numatm + id2];

                 dx = dx - xbox * (round(dx / xbox));
                 dy = dy - ybox * (round(dy / ybox));
                 dz = dz - zbox * (round(dz / zbox));

                 r = sqrtf(dx * dx + dy * dy + dz * dz);
                 if (r < cut)
                 {
                     ig2 = (int)(r / del);
                     #pragma acc atomic
                     dev_g2[ig2] = dev_g2[ig2] + 1;
                 }
             }
         }
     }
         
    }        
        
}


int main(int argc, char *argv[])
{
    StartTimer();
    double     xbox, ybox, zbox;
    double    *host_x, *host_y, *host_z;
    unsigned long long int     *host_g2;
    int        nbin;
    int        numatm, nconf, inconf;
    string     file;
    double     tElapsed;
    std::ifstream infile;

    inconf = 10;
    nbin   = 2000;

    file   = "./alk.traj.dcd"; 
    
    infile.open(file.c_str());
    if (!infile)
    {
        cout << "Input file: " << file.c_str() << " not found\n";
        return 1;
    }
    assert(infile);
    
    read_header(&numatm, &nconf, infile);
    
    cout << "Dcd file has " << numatm << " atoms and " << nconf << " frames" << endl;
    if (inconf > nconf)
        cout << "nconf is reset to " << nconf << endl;
    else
    {
        nconf = inconf;
    }
    
    cout << "Calculating RDF for " << nconf << " frames" << endl;

    unsigned long long int sizef   = nconf * numatm * sizeof(double);
    unsigned long long int sizebin = nbin * sizeof(unsigned long long int);

    host_x  = (double *)malloc(sizef);
    host_y  = (double *)malloc(sizef);
    host_z  = (double *)malloc(sizef);
    host_g2 = (unsigned long long int *)malloc(sizebin);

    memset(host_g2, 0, sizebin);


   /* Reading coordinates from the file */ 
    
 
    double ax[numatm], ay[numatm], az[numatm];
    for (int i = 0; i < nconf; i++)
    {
        read_data(ax, ay, az, infile, numatm, xbox, ybox, zbox);
        for (int j = 0; j < numatm; j++)
        {
            host_x[i * numatm + j] = ax[j];
            host_y[i * numatm + j] = ay[j];
            host_z[i * numatm + j] = az[j];
        }
    }
     
    cout << "Input file reading is finished." << endl;    
    
    #pragma acc data copyin (host_x[:nconf * numatm], host_y[:nconf * numatm], host_z[:nconf * numatm]) copyout( host_g2 [:nbin])
    {
    kernel_pairwise_distance(host_x, host_y, host_z, host_g2, numatm, nconf, xbox, ybox, zbox, nbin);    
    }
 
    double pi, rho, norm, rl, ru, nideal;
    double g2[nbin];
    double r, gr, lngr, lngrbond, s2, s2bond;
    double box;
    double del; 

    pi = acos(-1.0l);
    rho = (numatm) / (xbox * ybox * zbox);
    norm = (4.0l * pi * rho) / 3.0l;
    
    s2 = 0.0l; 
    s2bond = 0.0l;
    
    box = min(xbox, ybox);
    box = min(box, zbox);
    
    del = box / (2.0l * nbin);
    
    
    for (int i = 0; i < nbin; i++)
    {
        rl = (i)*del;
        ru = rl + del;
        nideal = norm * (ru * ru * ru - rl * rl * rl);
        g2[i] = (double)host_g2[i] / ((double)nconf * (double)numatm * nideal);
        r = (i)*del;
       
        printf ( "%15.6lf %15.6lf |", ((double)(i + 0.5l) * del) , (double)g2[i] );
        if ( (i+1)%6 == 0 )
              cout<<"\n";
        
        if (r < 2.0l)
            gr = 0.0l;        
        else
            gr = g2[i];

        if (gr < 1e-5)
            lngr = 0.0l;
        else
            lngr = log(gr);

        if (g2[i] < 1e-6)
            lngrbond = 0.0l;
        else
            lngrbond = log(g2[i]);

        s2 = s2 - 2.0l * pi * rho * ((gr * lngr) - gr + 1.0l) * del * r * r;
        s2bond = s2bond - 2.0l * pi * rho * ((g2[i] * lngrbond) - g2[i] + 1.0l) * del * r * r;
    }  
    
    cout << endl;
    cout << "Freeing Host memory" << endl;
    free(host_x);
    free(host_y);
    free(host_z);
    free(host_g2);
    
    cout << endl;
    cout << "s2 value    : " << s2 << endl;
    cout << "s2bond value: " << s2bond << endl;

    cout << "Number of atoms processed: " << numatm << "\n";
    cout << "Number of confs processed: " << nconf << "\n";   

    tElapsed = GetTimer() / 1000.0;
    cout <<"Total Time: "<< tElapsed << "secs" << "\n";

    return 0;
}
